var express = require('express');
var router = express.Router();
var _ = require('lodash');

const axios = require('axios');
var key = 'e8ed534d';
let movies = [
   {
   	id: _.uniqueId(), 
   	mov : "Fight Club"
   }
];

/* GET users listing. */
router.get('/movies', function(req, res, next) {
	res.json(movies);
});

router.put('/movies/', function(req,res,next){
	const {mov} = req.body;
	axios.get('http://www.omdbapi.com/', {
	    params: {
	      t: mov,
	      apikey: key
	    }
	  })
	  .then(function (response) {
	  	//si omdb a trouve le film
	  	if (response.data.Response === "True") {
		  	const obj = {
				id: _.uniqueId(),
				mov: response.data.Title,
				duration : response.data.Runtime,
				yearOfRelease : response.data.Released,
				actors : response.data.Actors,
				poster : response.data.Poster,
				boxOffice : response.data.BoxOffice || null
				rottenTomatoesScore : null

			}

			const Tomatoes = _.find(response.data.Ratings,["Source","RottenTomatoes"]);
			if (Tomatoes != undefined) {
				obj.rottenTomatoesScore = parseInt(Tomatoes.Value);
			}


			movies.push(obj);
			res.json(obj);
		}
	  })
	  .catch(function (error) {
	    console.log(error);
	  });  
});

router.route('/movie/:id')
	.get(function(req, res, next) {
		const {id} = req.params;
		const movToFind = _.find(movies,["id", id]);
		res.json(movToFind);
	})
	.post(function(req, res, next) {
	  const {id} = req.params;
	  const {mov} = req.body;
	  const movToUpdate = _.find(movies,["id",id]);
	  movToUpdate.mov = mov;
	  res.json(movToUpdate);
	})
	.delete(function(req, res, next) {
		const{id}=req.params;
		_.remove(movies, ["id",id]);
	  	res.json({
	  		message:`just removed ${id} :(`
	  	})
	});

module.exports = router;
