const express = require("express");
const crudRouter = require("./src/crud");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/crud", crudRouter);

app.listen(3000, function() {
	console.log("Listening http://localhost:3000/");
});
